package main

import (
	"golang.org/x/exp/constraints"
)

type Rules[T constraints.Ordered] []func(T) bool

func (r Rules[T]) Validate(val T) bool {
	i := len(r) - 1
	for i != -1 && r[i](val) {
		i--
	}
	return i == -1
}
func IntRange(a, b int) func(int) bool {
	return func(val int) bool {
		return a <= val && val <= b
	}
}
func main() {
	ir3_7 := Rules[int]{IntRange(3, 7)}

	println(ir3_7.Validate(5)) //true
	println(ir3_7.Validate(9)) //false
}
